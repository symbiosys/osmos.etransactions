﻿
using DataAnnotationsExtensions;
using System;
using System.ComponentModel.DataAnnotations;
namespace Osmos.ETransactions.Samples.Web.Models
{
    public class SingleAuthorizationData
    {
        [Min(100)]
        public int Amount { get; set; }
        [Required, StringLength(16, MinimumLength=16)]
        public string CreditCardNumber { get; set; }
        [Required, StringLength(4, MinimumLength = 4)]
        public string CreditCardDateVal { get; set; }
        [Required, StringLength(3, MinimumLength = 3)]
        public string CreditCardCVV { get; set; }
    }

    public class SingleDebitData {
        public Guid TransactionId { get; set; } 
    }
}