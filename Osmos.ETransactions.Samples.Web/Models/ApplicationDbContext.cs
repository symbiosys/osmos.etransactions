﻿using Osmos.ETransactions.Samples.Web.Models.Entities;
using System.Data.Entity;

namespace Osmos.ETransactions.Samples.Web.Models
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext()
            : base("Defaultconnection")
        {

        }

        public DbSet<NumQuestion> NumQuestions { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}