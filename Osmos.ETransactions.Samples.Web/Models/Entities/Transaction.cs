﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osmos.ETransactions.Samples.Web.Models.Entities
{
    public class Transaction: Entity
    {
        public int Amount { get; set; }
        public int RequestNumber { get; set; }
        public int TransactionNumber { get; set; }
    }
}