﻿using System;

namespace Osmos.ETransactions.Samples.Web.Models.Entities
{
    public class Entity
    {
        public Entity()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
    }
}