﻿
namespace Osmos.ETransactions.Samples.Web.Models.Entities
{
    public class NumQuestion
    {
        public string Id { get; set; }
        public int Value { get; set; }
    }
}