namespace Osmos.ETransactions.Samples.Web.Migrations
{
    using Osmos.ETransactions.Samples.Web.Models.Entities;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Osmos.ETransactions.Samples.Web.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Osmos.ETransactions.Samples.Web.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            string lastestNumQuestionId = "LastestNumQuestion";
            var lastestNumQuestion = context.NumQuestions.FirstOrDefault(nq => nq.Id == lastestNumQuestionId);
            if (lastestNumQuestion == null) {
                context.NumQuestions.Add(new NumQuestion { 
                    Id = lastestNumQuestionId,
                    Value = 1
                });
                context.SaveChanges();
            }
        }
    }
}
