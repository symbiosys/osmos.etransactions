﻿using Osmos.ETransactions.Samples.Web.Helpers;
using Osmos.ETransactions.Samples.Web.Models;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;

namespace Osmos.ETransactions.Samples.Web.Controllers
{
    [RoutePrefix("api/gae")]
    public class GAEController : ApiController
    {
        private ApplicationDbContext _db = null;

        public GAEController()
        {
            _db = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (_db != null) {
                    _db.Dispose();
                    _db = null;
                }
            }

            base.Dispose(disposing);
        }

        [Route("singleauthorization")]
        public async Task<IHttpActionResult> SingleAuthorization(SingleAuthorizationData data) {

            if(data == null) return BadRequest("The data can not be null.");
            if(!ModelState.IsValid) return BadRequest("Bad Arguments.");

            int latestNumQuestion = await Tools.GetLatestNumQuestionAsync(_db);
            var transaction = await Tools.CreateTransactionAsync(_db);

            var result = await GAE.SingleAuthorizationAsync(
                    latestNumQuestion, data.Amount, transaction.Id.ToString(), data.CreditCardNumber, data.CreditCardDateVal, data.CreditCardCVV);

            transaction.Amount = data.Amount;
            transaction.RequestNumber = result.NumAppel;
            transaction.TransactionNumber = result.NumTrans;
            await _db.SaveChangesAsync();

            await Tools.IncrementLatestNumQuestionAsync(_db);

            return Ok(result);
        }

        [Route("singledebit")]
        public async Task<IHttpActionResult> SingleDebit(SingleDebitData data) {
            if (data == null) return BadRequest("The data can not be null.");
            if (!ModelState.IsValid) return BadRequest("Bad Arguments.");

            int latestNumQuestion = await Tools.GetLatestNumQuestionAsync(_db);
            var transaction = await _db.Transactions.FirstOrDefaultAsync(t => t.Id == data.TransactionId);
            if (transaction == null) return NotFound();

            var result = await GAE.SingleDebitAsync(
                latestNumQuestion, transaction.Amount, transaction.Id.ToString(), transaction.RequestNumber, transaction.TransactionNumber);

            await Tools.IncrementLatestNumQuestionAsync(_db);

            return Ok(result);
        }
    }
}
