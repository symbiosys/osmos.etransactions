﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace Osmos.ETransactions.Samples.Web.Controllers
{
    [RoutePrefix("api/etransactions")]
    public class ETransactionsController : ApiController
    {
        [Route("paymentformparams")]
        public IHttpActionResult GetPaymentFromParams([FromUri] ETransactionsQuery query) {

            if (query == null) return BadRequest("The query can not be null.");
            if (!ModelState.IsValid) return BadRequest("Bad query.");
            if (Convert.ToInt32(query.Amount) <= 0) return BadRequest("The amount can not be zero or negative.");

            var result = new ETransactionsResult(query.Amount, query.CmdId, query.AbonneId);

            return Ok(result);
        }

        [Route("success")]
        public IHttpActionResult GetSuccess() {
            var queryString = Request.GetQueryNameValuePairs();

            string baseUrl = Request.RequestUri.Scheme + "://" + Request.RequestUri.Host + ":" +
    Request.RequestUri.Port + "/";

            return Redirect(baseUrl);
        }
    }
}
