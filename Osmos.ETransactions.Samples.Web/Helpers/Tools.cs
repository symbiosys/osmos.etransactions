﻿
using Osmos.ETransactions.Samples.Web.Models;
using Osmos.ETransactions.Samples.Web.Models.Entities;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Osmos.ETransactions.Samples.Web.Helpers
{
    public static class Tools
    {
        private static string _lastestNumQuestionId = "LastestNumQuestion";

        public static async Task<int> GetLatestNumQuestionAsync(ApplicationDbContext db) {
            var latestNumQuestionFromDb = await db.NumQuestions.FirstOrDefaultAsync(nq => nq.Id == _lastestNumQuestionId);
            if (latestNumQuestionFromDb == null) throw new Exception("Can not find the latest numQuestion.");
            return latestNumQuestionFromDb.Value;
        }

        public static async Task<Transaction> CreateTransactionAsync(ApplicationDbContext db)
        {
            var transaction = new Transaction { };
            db.Transactions.Add(transaction);
            await db.SaveChangesAsync();
            return transaction;
        }

        public static async Task IncrementLatestNumQuestionAsync(ApplicationDbContext db)
        {
            var latestNumQuestionFromDb = await db.NumQuestions.FirstOrDefaultAsync(nq => nq.Id == _lastestNumQuestionId);
            if (latestNumQuestionFromDb == null) throw new Exception("Can not find the latest numQuestion.");
            latestNumQuestionFromDb.Value++;
            await db.SaveChangesAsync();
        }
    }
}