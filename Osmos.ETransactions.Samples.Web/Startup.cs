﻿using Newtonsoft.Json.Serialization;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace Osmos.ETransactions.Samples.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {

            // web api configuration
            var config = new HttpConfiguration();
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.MapHttpAttributeRoutes();

            app.UseWebApi(config);
        }
    }
}