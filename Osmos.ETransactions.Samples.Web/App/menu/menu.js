﻿(function () {
    angular.module('app').controller('menuCtrl', ['$route', menuCtrl]);

    function menuCtrl($route) {

        var vm = this;

        vm.getClass = function (route) {
            if ($route.current && $route.current.$$route && $route.current.$$route.originalPath) {
                console.log($route.current.$$route.originalPath);
                console.log(route);
                console.log('-------------------------');
                if (route == $route.current.$$route.originalPath) return 'active';
            }
            return '';
        };
    }
}());