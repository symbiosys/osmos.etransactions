﻿(function () {
    angular.module('app').controller('eTransactionsFormCtrl', ['$http', '$modal', eTransactionsFormCtrl]);
    function eTransactionsFormCtrl($http, $modal) {

        var vm = this;

        vm.openConfirmPaymentModal = function () {

            if (!vm.amount || !vm.cmdId || !vm.abonneId) return;

            var modalInstance = $modal.open({
                templateUrl: 'confirmPaymentModal.html',
                controller: 'confirmPaymentModalInstanceCtrl as vm',
                size: 'sm',
                resolve: {
                    amount: function () {
                        return vm.amount;
                    },
                    cmdId: function () {
                        return vm.cmdId;
                    },
                    abonneId: function () {
                        return vm.abonneId;
                    }
                }
            });
        }
    }

    angular.module('app').controller('confirmPaymentModalInstanceCtrl',
        ['$modalInstance', '$http', '$sce', 'amount', 'cmdId', 'abonneId',
        function ($modalInstance, $http, $sce, amount, cmdId, abonneId) {

            var vm = this;

            vm.cancel = function () {
                $modalInstance.close();
            }

            getPaymentFromParams();

            function getPaymentFromParams() {

                var queryObject = {
                    amount: amount,
                    cmdId: cmdId,
                    abonneId: abonneId
                };

                var url = '/api/etransactions/paymentformparams?' + $.param(queryObject);
                return $http({
                    url: url,
                    method: 'get'
                })
                 .success(function (data) {
                     data.url = $sce.trustAsResourceUrl(data.url);
                     vm.paymentFormParams = data;
                     console.log(vm.paymentFormParams);
                 })
                 .error(function (data) {
                     console.error(data);
                     $modalInstance.close();
                 });
            }
        }]);

}());