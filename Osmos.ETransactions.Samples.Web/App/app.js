﻿(function () {
    angular.module('app', ['ui.bootstrap', 'ngRoute']);

    angular.module('app').config(['$routeProvider',
    function ($routeProvider) {

        $routeProvider
            .when('/etransationsform',
                {
                    templateUrl: 'App/modules/eTransactionsForm/eTransactionsForm.html',
                    controller: 'eTransactionsFormCtrl as vm',
                    name: 'ETransactionsForm'
                })
            .when('/gae',
                {
                    templateUrl: 'App/modules/gAE/gAE.html',
                    controller: 'gAECtrl as vm',
                    name: 'GestionAutomatiseeDesEncaissements'
                })
            .otherwise({ redirectTo: '/etransationsform' });

    }]);
}());