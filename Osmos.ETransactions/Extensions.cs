﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Osmos.ETransactions
{
    static class Extensions
    {
        private static string invalidStringMessage = "This is not a valid string.";

        private static Dictionary<string, string> ToKeyValuePairs(this string str) { 
            var pairs = str.Split('&');
            if (!pairs.Any()) throw new Exception(invalidStringMessage);

            var dictionary = new Dictionary<string, string>();
            foreach (string pair in pairs)
            {
                var keyValue = pair.Split('=');
                if (keyValue.Length != 2) throw new Exception(invalidStringMessage);
                dictionary.Add(keyValue[0], keyValue[1]);
            }
            return dictionary;
        }

        public static GAEResult ToGAEResult(this string str, string requestMessage)
        {
            var responseValues = str.ToKeyValuePairs();

            bool test = responseValues.ContainsKey("NUMTRANS") &&
                responseValues.ContainsKey("NUMAPPEL") &&
                responseValues.ContainsKey("NUMQUESTION") &&
                responseValues.ContainsKey("SITE") &&
                responseValues.ContainsKey("RANG") &&
                responseValues.ContainsKey("AUTORISATION") &&
                responseValues.ContainsKey("CODEREPONSE") &&
                responseValues.ContainsKey("COMMENTAIRE") &&
                responseValues.ContainsKey("REFABONNE") &&
                responseValues.ContainsKey("PORTEUR");
            if (!test) throw new Exception("Can not parse the string into a GAEResult object.");

            return new GAEResult
            {
                NumTrans = Convert.ToInt32(responseValues["NUMTRANS"]),
                NumAppel = Convert.ToInt32(responseValues["NUMAPPEL"]),
                NumQuestion = Convert.ToInt32(responseValues["NUMQUESTION"]),
                Site = responseValues["SITE"],
                Rang = responseValues["RANG"],
                Autorisation = responseValues["AUTORISATION"],
                CodeReponse = responseValues["CODEREPONSE"],
                Commentaire = responseValues["COMMENTAIRE"],
                RefAbonne = responseValues["REFABONNE"],
                Porteur = responseValues["PORTEUR"],
                ResponseMessage = str,
                RequestMessage = requestMessage
            };
        }
    }
}
