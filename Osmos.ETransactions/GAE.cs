﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;

namespace Osmos.ETransactions
{
    public class GAEResult
    {
        public int NumTrans { get; set; }
        public int NumAppel { get; set; }
        public int NumQuestion { get; set; }
        public string Site { get; set; }
        public string Rang { get; set; }
        public string Autorisation { get; set; }
        public string CodeReponse { get; set; }
        public string Commentaire { get; set; }
        public string RefAbonne { get; set; }
        public string Porteur { get; set; }
        public string ResponseMessage { get; set; }
        public string RequestMessage { get; set; }
    }

    public static class GAE
    {
        private static async Task<GAEResult> _SendRequest(string message)
        {
            string hmac = ConfigurationManager.AppSettings["PBX_HMAC"];
            string hmacHash = Hash.GetSHA512(message, hmac);

            string contentStr = string.Format("{0}&HMAC={1}", message, hmacHash);
            var request = new HttpRequestMessage(HttpMethod.Post, ConfigurationManager.AppSettings["URL_GAE"]);
            request.Content = new StringContent(contentStr);
            var httpClient = new HttpClient();
            try
            {
                var reponse = await httpClient.SendAsync(request);
                reponse.EnsureSuccessStatusCode();

                string responseStr = await reponse.Content.ReadAsStringAsync();
                httpClient.Dispose();
                return responseStr.ToGAEResult(contentStr);
            }
            catch (Exception e)
            {
                httpClient.Dispose();
                throw e;
            }
        }

        private static string _CreateMessageForSingleAuthorization(
            int numQuestion,
            int montant,
            string reference,
            string porteur,
            string dateVal,
            string cvv)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00001";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //montant
            string devise = "978";
            //reference
            //porteur
            //dateVal
            //cvv

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&MONTANT={8}" +
                "&DEVISE={9}" +
                "&REFERENCE={10}" +
                "&PORTEUR={11}" +
                "&DATEVAL={12}" +
                "&CVV={13}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                montant,
                devise,
                reference,
                porteur,
                dateVal,
                cvv);

            return message;
        }

        private static string _CreateMessageForSingleDebit(
            int numQuestion, int montant, string reference, int numAppel, int numTrans)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00002";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //montant
            string devise = "978";
            //numAppel
            //numTrans

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&MONTANT={8}" +
                "&REFERENCE={9}" +
                "&DEVISE={10}" +
                "&NUMAPPEL={11}" +
                "&NUMTRANS={12}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                montant,
                reference,
                devise,
                numAppel,
                numTrans);

            return message;
        }

        private static string _CreateMessageForSingleAuthorizationAndDebit(
            int numQuestion,
            int montant,
            string reference,
            string porteur,
            string dateVal,
            string cvv)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00003";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //montant
            string devise = "978";
            //reference
            //porteur
            //dateVal
            //cvv

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&MONTANT={8}" +
                "&DEVISE={9}" +
                "&REFERENCE={10}" +
                "&PORTEUR={11}" +
                "&DATEVAL={12}" +
                "&CVV={13}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                montant,
                devise,
                reference,
                porteur,
                dateVal,
                cvv);

            return message;
        }

        private static string _CreateMessageForSubscriptionCreation(
            int numQuestion,
            int montant,
            string refabonne,
            string porteur,
            string dateVal,
            string cvv)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00056";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            //montant
            string devise = "978";
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //porteur
            //dateVal
            //cvv

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&MONTANT={7}" +
                "&DEVISE={8}" +
                "&CLE={9}" +
                "&REFABONNE={10}" +
                "&PORTEUR={11}" +
                "&DATEVAL={12}" +
                "&CVV={13}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                montant,
                devise,
                cle,
                refabonne,
                porteur,
                dateVal,
                cvv);

            return message;
        }

        private static string _CreateMessageForSubscriptionDeletion(
            int numQuestion,
            string reference,
            string refAbonne,
            string porteur,
            string dateVal)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00058";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //reference
            //refAbonne
            string activite = "027";
            //porteur
            //dateVal

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&REFERENCE={8}" +
                "&REFABONNE={9}" +
                "&ACTIVITE={10}" +
                "&PORTEUR={11}" +
                "&DATEVAL={12}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                reference,
                refAbonne,
                activite,
                porteur,
                dateVal);

            return message;
        }

        private static string _CreateMessageForSubscribtionAuthorization(
            int numQuestion, 
            int montant, 
            string reference,
            string refAbonne,
            string porteur,
            string dateVal)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00051";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //montant
            //reference
            string devise = "978";
            //refAbonne
            string activite = "027";
            //porteur
            //dateVal

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&MONTANT={8}" +
                "&REFERENCE={9}" +
                "&DEVISE={10}" +
                "&REFABONNE={11}" +
                "&ACTIVITE={12}" +
                "&PORTEUR={13}" +
                "&DATEVAL={14}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                montant,
                reference,
                devise,
                refAbonne,
                activite,
                porteur,
                dateVal);

            return message;
        }

        private static string _CreateMessageForSubscribtionDebit(
            int numQuestion,
            int montant,
            string reference,
            int numAppel,
            int numTrans)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00051";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //montant
            //reference
            string devise = "978";
            string activite = "027";
            //numAppel
            //numTrans

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&MONTANT={8}" +
                "&REFERENCE={9}" +
                "&DEVISE={10}" +
                "&ACTIVITE={12}" +
                "&NUMAPPEL={11}" +
                "&NUMTRANS={12}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                montant,
                reference,
                devise,
                activite,
                numAppel,
                numTrans);

            return message;
        }

        private static string _CreateMessageForSubscribtionAuthorizationAndDebit(
            int numQuestion,
            int montant,
            string reference,
            string refAbonne,
            string porteur,
            string dateVal)
        {
            string identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            string site = ConfigurationManager.AppSettings["PBX_SITE"];
            string rang = ConfigurationManager.AppSettings["PBX_RANG"];
            string type = "00053";
            string version = "00104";
            string dateQ = DateTime.UtcNow.ToString("ddMMyyyyHHmmss", CultureInfo.InvariantCulture);
            //numQuestion
            string cle = ConfigurationManager.AppSettings["PBX_CLE"];
            //montant
            //reference
            string devise = "978";
            //refAbonne
            string activite = "027";
            //porteur
            //dateVal

            string message = string.Format(
                "IDENTIFIANT={0}" +
                "&SITE={1}" +
                "&RANG={2}" +
                "&VERSION={3}" +
                "&TYPE={4}" +
                "&DATEQ={5}" +
                "&NUMQUESTION={6}" +
                "&CLE={7}" +
                "&MONTANT={8}" +
                "&REFERENCE={9}" +
                "&DEVISE={10}" +
                "&REFABONNE={11}" +
                "&ACTIVITE={12}" +
                "&PORTEUR={13}" +
                "&DATEVAL={14}",
                identifiant,
                site,
                rang,
                version,
                type,
                dateQ,
                numQuestion,
                cle,
                montant,
                reference,
                devise,
                refAbonne,
                activite,
                porteur,
                dateVal);

            return message;
        }

        public static async Task<GAEResult> SingleAuthorizationAsync(
            int numQuestion,
            int montant,
            string reference,
            string porteur,
            string dateVal,
            string cvv)
        {
            string message = _CreateMessageForSingleAuthorization(numQuestion, montant, reference, porteur, dateVal, cvv);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SingleDebitAsync(int numQuestion, int montant, string reference, int numAppel, int numTrans)
        {
            string message = _CreateMessageForSingleDebit(numQuestion, montant, reference, numAppel, numTrans);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SingleAuthorizationAndDebitAsync(
            int numQuestion,
            int montant,
            string reference,
            string porteur,
            string dateVal,
            string cvv)
        {
            string message = _CreateMessageForSingleAuthorizationAndDebit(numQuestion, montant, reference, porteur, dateVal, cvv);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SubscriptionCreationAsync(
            int numQuestion,
            int montant,
            string refabonne,
            string porteur,
            string dateVal,
            string cvv)
        {
            string message = _CreateMessageForSubscriptionCreation(numQuestion, montant, refabonne, porteur, dateVal, cvv);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SubscriptionDeletionAsync(
            int numQuestion,
            string reference,
            string refAbonne,
            string porteur,
            string dateVal)
        {
            string message = _CreateMessageForSubscriptionDeletion(numQuestion, reference, refAbonne, porteur, dateVal);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SubscribtionAuthorizationAsync(
            int numQuestion,
            int montant,
            string reference,
            string refAbonne,
            string porteur,
            string dateVal)
        {
            string message = _CreateMessageForSubscribtionAuthorization(numQuestion, montant, reference, refAbonne, porteur, dateVal);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SubscribtionDebitAsync(
            int numQuestion,
            int montant,
            string reference,
            int numAppel,
            int numTrans)
        {
            string message = _CreateMessageForSingleDebit(numQuestion, montant, reference, numAppel, numTrans);
            return await _SendRequest(message);
        }

        public static async Task<GAEResult> SubscribtionAuthorizationAndDebitAsync(
            int numQuestion,
            int montant,
            string reference,
            string refAbonne,
            string porteur,
            string dateVal)
        {
            string message = _CreateMessageForSubscribtionAuthorizationAndDebit(numQuestion, montant, reference, refAbonne, porteur, dateVal);
            return await _SendRequest(message);
        }
    }
}
