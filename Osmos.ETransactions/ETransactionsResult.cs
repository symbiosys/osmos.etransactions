﻿
using System;
using System.Configuration;
namespace Osmos.ETransactions
{
    public class ETransactionsResult
    {
        public ETransactionsResult(string amount, string cmdId, string abonneId)
        {
            Url = ConfigurationManager.AppSettings["URL_PAYMENT"];
            Site = ConfigurationManager.AppSettings["PBX_SITE"];
            Rang = ConfigurationManager.AppSettings["PBX_RANG"];
            Identifiant = ConfigurationManager.AppSettings["PBX_IDENTIFIANT"];
            Total = amount;
            Devise = ConfigurationManager.AppSettings["PBX_DEVISE"];
            Cmd = cmdId;
            Abonne = abonneId;
            Porteur = ConfigurationManager.AppSettings["PBX_PORTEUR"];
            Retour = ConfigurationManager.AppSettings["PBX_RETOUR"];
            Hash = "SHA512";
            Time = DateTime.UtcNow.ToString("o");
            HmacHash = ETransactions.GetHashedString(Total, cmdId, abonneId, Time);
        }

        public string Url { get; set; }
        public string Site { get; set; }
        public string Rang { get; set; }
        public string Identifiant { get; set; }
        public string Total { get; set; }
        public string Devise { get; set; }
        public string Cmd { get; set; }
        public string Abonne { get; set; }
        public string Porteur { get; set; }
        public string Retour { get; set; }
        public string Hash { get; set; }
        public string Time { get; set; }
        public string HmacHash { get; set; }
    }
}
