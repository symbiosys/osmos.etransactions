﻿using System.ComponentModel.DataAnnotations;

namespace Osmos.ETransactions
{
    public class ETransactionsQuery
    {
        [Required]
        public string Amount { get; set; }
        [Required]
        public string CmdId { get; set; }
        [Required]
        public string AbonneId { get; set; }
    }
}
