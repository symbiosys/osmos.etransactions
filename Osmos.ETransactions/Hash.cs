﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Osmos.ETransactions
{
    class Hash
    {
        private static string _ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2");
            return sbinary;
        }

        private static byte[] _PackH(string hex)
        {
            byte[] raw = new byte[hex.Length / 2];
            for (int i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return raw;
        }

        public static string GetSHA512(string text, string key)
        {
            byte[] byteArr = _PackH(key);
            Encoding encoding = Encoding.UTF8;
            var keyByte = key;
            string hex = "";
            using (var hmacsha512 = new HMACSHA512(byteArr))
            {
                hmacsha512.ComputeHash(encoding.GetBytes(text));


                hex = _ByteToString(hmacsha512.Hash);
            }

            return hex;
        }
    }
}
