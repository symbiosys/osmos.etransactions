﻿using System.Configuration;

namespace Osmos.ETransactions
{
    static class ETransactions
    {
        private static string GetStringToHash(string amount, string cmdId, string abonneId, string time) {
            string str = "PBX_SITE={0}" +
                            "&PBX_RANG={1}" +
                            "&PBX_IDENTIFIANT={2}" +
                            "&PBX_TOTAL={3}" +
                            "&PBX_DEVISE={4}" +
                            "&PBX_CMD={5}" +
                            "&PBX_REFABONNE={6}" +
                            "&PBX_PORTEUR={7}" + 
                            "&PBX_RETOUR={8}" + 
                            "&PBX_HASH=SHA512" +
                            "&PBX_TIME={9}";

            return string.Format(str,
                    ConfigurationManager.AppSettings["PBX_SITE"],
                    ConfigurationManager.AppSettings["PBX_RANG"],
                    ConfigurationManager.AppSettings["PBX_IDENTIFIANT"],
                    amount,
                    ConfigurationManager.AppSettings["PBX_DEVISE"],
                    cmdId,
                    abonneId,
                    ConfigurationManager.AppSettings["PBX_PORTEUR"],
                    ConfigurationManager.AppSettings["PBX_RETOUR"],
                    time
                );
        }

        public static string GetHashedString(string amount, string cmdId, string abonneId, string time)
        {
            string str = GetStringToHash(amount, cmdId, abonneId, time);
            return Hash.GetSHA512(str, ConfigurationManager.AppSettings["PBX_HMAC"]);
        }
    }
}
